import requests
import json
from string import Template


class GitLabPipeLine:
    url = ""
    token = ""
    pipeLineUrl = Template('https://gitlab.com/api/v4/projects/$n1/pipelines')

    def __init__(self, repoId, token):
        self.repoId = repoId
        self.token = token
        self.url = self.pipeLineUrl.substitute(n1=self.repoId)

    def GetLatestStatus(self):
        headers = {'PRIVATE-TOKEN': self.token}
        res = requests.get(self.url, headers=headers)

        pipelines = json.loads(res.text)
        pipeline = pipelines[0]

        return pipeline['status']
