# !/usr/bin/env python
import time
from sys import exit
import unicornhathd

try:
    from PIL import Image, ImageDraw, ImageFont
except ImportError:
    exit('This script requires the pillow module\nInstall with: sudo pip install pillow')

unicornhathd.rotation(270)
unicornhathd.brightness(0.6)


_r = 0
_g = 0
_b = 0
_FONT = ('/usr/share/fonts/truetype/freefont/FreeSansBold.ttf', 12)

def ShowSuccess():
    _r=0
    _g-255
    _b = 0
    displayText("Build has been Successful")

def ShowFailure():
    r = 255
    _g - 0
    _b = 0
    displayText("Build has been Successful")


def displayText(message):


    width, height = unicornhathd.get_shape()

    text_x = width
    text_y = 2

    font_file, font_size = _FONT

    font = ImageFont.truetype(font_file, font_size)

    text_width, text_height = width, 0

    try:
            w, h = font.getsize(message)
            text_width += w + width
            text_height = max(text_height, h)

            text_width += width + text_x + 1

            image = Image.new('RGB', (text_width, max(16, text_height)), (0, 0, 0))
            draw = ImageDraw.Draw(image)

            offset_left = 0


            draw.text((text_x + offset_left, text_y), message, fill=(_r,_g,_b,255), font=font)

            offset_left += font.getsize(message)[0] + width

            for scroll in range(text_width - width):
                for x in range(width):
                    for y in range(height):
                        #pixel = image.getpixel((x + scroll, y))

                        unicornhathd.set_pixel(width - 1 - x, y, _r, _g, _b)

                unicornhathd.show()
                time.sleep(0.01)

    except KeyboardInterrupt:
        unicornhathd.off()

    finally:
        unicornhathd.off()


