from gitlabapi import GitLabPipeLine
from LedHat import LedDisplay

repoId = '' # add your repo Id here
token = '' # add your Auth token here
message = ' Add a message here $n2  '

gitlabData = GitLabPipeLine(repoId, token)
ledDisplay = LedDisplay(message)

status = gitlabData.GetLatestStatus()

if status == 'success':
    ledDisplay.ShowSuccess()
else:
    ledDisplay.ShowFailure()
